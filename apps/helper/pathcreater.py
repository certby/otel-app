from __future__ import unicode_literals

from os.path import join, splitext


def get_file_path(instance, filename):
    from apps.models import Home, HomeSlider, Blogs, GalleryImage, RoomImage,AboutSlider, ContactSlider, RoomService, RoomSlider, Restaurant, \
        RestaurantService, RestaurantSlider, MeetingRoom, MeetingRoomService, MeetingRoomGallery, MeetingRoomSlider, \
        SpaFitnessImage, \
        SpaFitnessSlider, SpaFitnessProperties, \
        SpaFitnessService, About, Room, Gallery, RestaurantMenu, BlogSlider,DocumentsSlider, GallerySlider, BlogPageSlider
    lastPath = splitext(filename)[1][1:].lower()
    instanceType = type(instance)

    if instanceType is Home:
        return 'home/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is HomeSlider:
        return 'home_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is AboutSlider:
        return 'about_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is BlogSlider:
        return 'blog_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is About:
        return 'about/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is BlogPageSlider:
        return 'about/{}/blog_page_slider/{}'.format(str(instance.id), filename)
    elif instanceType is ContactSlider:
        return 'contact_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RestaurantMenu:
        return 'restaurant_menu/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RestaurantSlider:
        return 'restaurant_image/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MeetingRoom:
        return 'meeting_room/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MeetingRoomService:
        return 'meeting_room_service/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MeetingRoomGallery:
        return 'meeting_room_gallery/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MeetingRoomSlider:
        return 'meeting_room_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is SpaFitnessImage:
        return 'spa_fitness_image/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is SpaFitnessSlider:
        return 'spa_fitness_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is SpaFitnessProperties:
        return 'spa_fitness_properties/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is SpaFitnessService:
        return 'spa_fitness_service/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Blogs:
        return 'blogs/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is GallerySlider:
        return 'gallery_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Gallery:
        return 'Gallery/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is GalleryImage:
        return 'gallery_image/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RoomImage:
        return 'room_image/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RoomService:
        return 'room_service/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Restaurant:
        return 'restaurant/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RestaurantService:
        return 'restaurant_service/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Room:
        return 'room/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is RoomSlider:
        return 'room_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is DocumentsSlider:
        return 'document_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Gallery:
        return 'gallery/{}/image/{}'.format(str(instance.id), filename)
    return join(instance, filename)


def get_upload_path(instance, filename):
    from apps.models import MeetingRoom, Restaurant
    instanceType = type(instance)

    if instanceType is MeetingRoom:
        return 'meeting_room/{}/file/{}'.format(str(instance.id), filename)
    elif instanceType is Restaurant:
        return 'restaurant_menu/{}/image/{}'.format(str(instance.id), filename)
    return join(instance, filename)
