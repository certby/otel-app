from modeltranslation.translator import translator, TranslationOptions
from .models import *
from sitetree.models import TreeItem


class ContactTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)


class AboutTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'short_description',)


class RoomTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'short_description')


class RestaurantTranslationOptions(TranslationOptions):
    fields = ('title', 'small_title', 'description', 'short_description',)


class RoomSliderTranslationOptions(TranslationOptions):
    fields = ('title',)


class RoomServiceTranslationOptions(TranslationOptions):
    fields = ('title',)


class RestaurantMenuTranslationOptions(TranslationOptions):
    fields = ('title', 'sort_description',)


class RestaurantServiceTranslationOptions(TranslationOptions):
    fields = ('title',)


class MeetingRoomTranslationOptions(TranslationOptions):
    fields = ('title', 'small_title', 'description', 'short_description',)


class MeetingRoomSliderTranslationOptions(TranslationOptions):
    fields = ('title',)


class MeetingRoomServiceTranslationOptions(TranslationOptions):
    fields = ('title',)


class SpaAndFitnessTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)


class BlogsTranslationOptions(TranslationOptions):
    fields = ('small_title', 'title', 'description', 'short_description')


class LiveSupportTranslationOptions(TranslationOptions):
    fields = ('title',)


class SpaFitnessServiceTranslationOptions(TranslationOptions):
    fields = ('title',)


class SpaFitnessPropertiesTranslationOptions(TranslationOptions):
    fields = ('title',)


class DocumentsTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)


class TreeItemTranslationOptions(TranslationOptions):
    fields = ('title', 'url',)


translator.register(Contact, ContactTranslationOptions)
translator.register(About, AboutTranslationOptions)
translator.register(Room, RoomTranslationOptions)
translator.register(RoomService, RoomServiceTranslationOptions)
translator.register(Restaurant, RestaurantTranslationOptions)
translator.register(RestaurantMenu, RestaurantMenuTranslationOptions)
translator.register(RestaurantService, RestaurantServiceTranslationOptions)
translator.register(MeetingRoom, MeetingRoomTranslationOptions)
translator.register(MeetingRoomService, MeetingRoomServiceTranslationOptions)
translator.register(SpaAndFitness, SpaAndFitnessTranslationOptions)
translator.register(Blogs, BlogsTranslationOptions)
translator.register(LiveSupport, LiveSupportTranslationOptions)
translator.register(Documents, DocumentsTranslationOptions)
translator.register(SpaFitnessService, SpaFitnessServiceTranslationOptions)
translator.register(SpaFitnessProperties, SpaFitnessPropertiesTranslationOptions)
translator.register(TreeItem, TreeItemTranslationOptions)
