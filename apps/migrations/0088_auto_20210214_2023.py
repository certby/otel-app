# Generated by Django 2.2.18 on 2021-02-14 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0087_auto_20210214_2009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurantreservation',
            name='message',
            field=models.TextField(editable=False),
        ),
    ]
