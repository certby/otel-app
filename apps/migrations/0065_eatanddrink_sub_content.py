# Generated by Django 2.2.18 on 2021-02-11 21:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0064_auto_20210211_2358'),
    ]

    operations = [
        migrations.AddField(
            model_name='eatanddrink',
            name='sub_content',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
