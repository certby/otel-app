# Generated by Django 2.2.17 on 2021-01-10 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0031_auto_20210110_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='is_home',
            field=models.BooleanField(default=True),
        ),
    ]
