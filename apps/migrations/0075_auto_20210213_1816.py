# Generated by Django 2.2.18 on 2021-02-13 15:16

import apps.helper.pathcreater
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0074_auto_20210213_1803'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Menu',
            new_name='RestaurantMenu',
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='logo',
            field=models.FileField(blank=True, null=True, upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Logo'),
        ),
    ]
