# Generated by Django 2.2.18 on 2021-02-13 19:26

import apps.helper.pathcreater
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0078_restaurantreservation'),
    ]

    operations = [
        migrations.CreateModel(
            name='MeetingRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(blank=True, max_length=100, verbose_name='Başlık')),
                ('slug', models.SlugField(max_length=40)),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Açıklama')),
            ],
            options={
                'verbose_name_plural': 'RESTORANT',
                'db_table': 'meeting',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='MeetingRoomService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('title', models.CharField(max_length=60)),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Toplantı Servis Resim')),
                ('meeting', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.MeetingRoom')),
            ],
        ),
        migrations.CreateModel(
            name='MeetingRoomGallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('title', models.CharField(max_length=60)),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Toplantı Servis Resim')),
                ('meeting', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.MeetingRoom')),
            ],
        ),
    ]
