# Generated by Django 2.2.18 on 2021-02-14 12:16

import apps.helper.pathcreater
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0080_auto_20210213_2322'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpaAndFitness',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
            ],
            options={
                'verbose_name_plural': 'SPA & FITNESS',
                'db_table': 'spa_fitness',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='SpaFitnessSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('title', models.CharField(max_length=60)),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Spa & Fitness Resim')),
                ('spa_fitness', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.SpaAndFitness')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SpaFitnessService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('title', models.CharField(max_length=60)),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Spa & Fitness Servis Resim')),
                ('spa_fitness', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.SpaAndFitness')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SpaFitnessProperties',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('title', models.CharField(max_length=60)),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Spa & Fitness Özellik Resim')),
                ('spa_fitness', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.SpaAndFitness')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
