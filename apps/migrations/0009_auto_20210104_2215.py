# Generated by Django 2.2.17 on 2021-01-04 19:15

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0008_auto_20210104_2134'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contact',
            old_name='email',
            new_name='email1',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='address',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='city',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='fax',
        ),
        migrations.AddField(
            model_name='contact',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contact',
            name='email2',
            field=models.EmailField(default=1, max_length=50),
            preserve_default=False,
        ),
    ]
