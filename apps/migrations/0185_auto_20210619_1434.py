# Generated by Django 2.1.15 on 2021-06-19 11:34

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0184_auto_20210619_1323'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='about',
            name='other',
        ),
        migrations.RemoveField(
            model_name='blogs',
            name='other',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='other',
        ),
        migrations.RemoveField(
            model_name='gallery',
            name='other',
        ),
        migrations.RemoveField(
            model_name='home',
            name='other',
        ),
        migrations.RemoveField(
            model_name='meetingroom',
            name='other',
        ),
        migrations.RemoveField(
            model_name='restaurant',
            name='other',
        ),
        migrations.RemoveField(
            model_name='room',
            name='other',
        ),
        migrations.RemoveField(
            model_name='spaandfitness',
            name='other',
        ),
        migrations.AddField(
            model_name='about',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='contact',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='home',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='meetingroom',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='room',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
        migrations.AddField(
            model_name='spaandfitness',
            name='other_metas',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar'),
        ),
    ]
