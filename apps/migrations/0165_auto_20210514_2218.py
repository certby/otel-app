# Generated by Django 2.1.15 on 2021-05-14 19:18

import apps.helper.pathcreater
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0164_remove_spafitnessslider_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='spafitnessimage',
            name='title',
        ),
        migrations.AlterField(
            model_name='spafitnessimage',
            name='image',
            field=models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Resim'),
        ),
        migrations.AlterField(
            model_name='spafitnessslider',
            name='image',
            field=models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Resim'),
        ),
    ]
