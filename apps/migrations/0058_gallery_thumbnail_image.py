# Generated by Django 2.2.17 on 2021-01-17 14:40

import apps.helper.pathcreater
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0057_auto_20210117_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='thumbnail_image',
            field=models.FileField(default=1, upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Küçük Resim'),
            preserve_default=False,
        ),
    ]
