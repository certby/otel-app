# Generated by Django 2.2.18 on 2021-03-03 07:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0098_livesupport_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogs',
            name='slug',
            field=models.SlugField(default=1, max_length=40, verbose_name='Seo'),
            preserve_default=False,
        ),
    ]
