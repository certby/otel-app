# Generated by Django 2.2.18 on 2021-02-11 21:19

import apps.helper.pathcreater
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0065_eatanddrink_sub_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ballroom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=60)),
                ('content', models.TextField()),
                ('image', models.ImageField(blank=True, upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Resim')),
            ],
            options={
                'verbose_name_plural': 'BALO SALONU',
                'db_table': 'ballroom',
                'ordering': ['-created_at'],
            },
        ),
    ]
