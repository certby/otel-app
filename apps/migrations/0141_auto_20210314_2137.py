# Generated by Django 2.2.18 on 2021-03-14 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0140_auto_20210314_2137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscribercontact',
            name='kvkk1',
            field=models.BooleanField(default=True, verbose_name='KVKK Metni Seçili Mi ?'),
        ),
        migrations.AlterField(
            model_name='subscribercontact',
            name='kvkk2',
            field=models.BooleanField(default=True, verbose_name='Açık Rıza Metni Seçili Mi ?'),
        ),
    ]
