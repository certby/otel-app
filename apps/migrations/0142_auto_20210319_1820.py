# Generated by Django 2.1.15 on 2021-03-19 15:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0141_auto_20210314_2137'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homeslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homeslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homeslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homeslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='livesupport',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='livesupport',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='livesupport',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='livesupport',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroom',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroom',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroom',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroom',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomgallery',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomgallery',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomgallery',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomgallery',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomservice',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomservice',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomservice',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomservice',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='meetingroomslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantmenu',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantmenu',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantmenu',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantmenu',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantreservation',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantreservation',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantreservation',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantreservation',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantservice',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantservice',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantservice',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantservice',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='restaurantslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='room',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='room',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='room',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='room',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomimage',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomimage',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomimage',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomimage',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomservice',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomservice',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomservice',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomservice',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='roomslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spaandfitness',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spaandfitness',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spaandfitness',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spaandfitness',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessimage',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessimage',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessimage',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessimage',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessproperties',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessproperties',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessproperties',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessproperties',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessservice',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessservice',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessservice',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessservice',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spafitnessslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spareservation',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spareservation',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spareservation',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='spareservation',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscribercontact',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscribercontact',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscribercontact',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscribercontact',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='userprofileinfo',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='userprofileinfo',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='userprofileinfo',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='userprofileinfo',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
    ]
