# Generated by Django 2.2.17 on 2021-01-10 17:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0036_socialmedia'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='about',
            name='is_home',
        ),
    ]
