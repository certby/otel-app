# Generated by Django 2.2.18 on 2021-03-03 09:19

import apps.helper.pathcreater
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0102_spareservation'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.FileField(upload_to=apps.helper.pathcreater.get_file_path, verbose_name='Resim')),
                ('restaurant', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.Restaurant')),
            ],
        ),
    ]
