import datetime

from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

from apps.models import Blogs, ACTIVE, MeetingRoom, Restaurant, Room


class StaticViewSitemap(Sitemap):
    def items(self):
        return [
            'home',
            'about',
            'galeri',
            'spa',
            'contact',
            'blog',
        ]

    def location(self, item):
        return reverse(item)


class BlogSitemap(Sitemap):
    def items(self):
        items = Blogs.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class MeetingRoomSitemap(Sitemap):
    def items(self):
        items = MeetingRoom.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class RoomSitemap(Sitemap):
    def items(self):
        items = Room.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class RestaurantSitemap(Sitemap):

    def items(self):
        items = Restaurant.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items
