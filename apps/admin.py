from django.contrib import admin
from django.contrib.admin import DateFieldListFilter, RelatedFieldListFilter
from django.contrib.auth.models import Group

from .forms import ModelWithImageFieldForm, ModelWithImageFieldForm2, AboutForm
from .models import *
from modeltranslation.admin import TranslationAdmin
from sitetree.admin import TreeItemAdmin, override_item_admin
from jet.admin import CompactInline
from jet.filters import DateRangeFilter
from django.forms import FileInput, ModelForm, TextInput, Textarea

admin.site.unregister(Group)


class AboutSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = AboutSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'SLIDER'
    extra = 0


@admin.register(About)
class AboutAdmin(admin.ModelAdmin):
    form = AboutForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [AboutSliderAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('image', 'image_alt_text'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'short_description_tr', 'content_tr', ),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'short_description_en', 'content_en', ),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    class Meta:
        model = About

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Social)
class SocialAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    list_display = ['order_no', 'title', 'icon', 'link']

    fieldsets = (
        ('Genel', {
            'fields': ('order_no', 'title', 'icon', 'link',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    class Meta:
        model = Social


class ContactSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = ContactSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'SLIDER'
    extra = 0


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    inlines = [ContactSliderAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('check_in', 'check_out', 'city', 'address', 'country_code', 'phone', 'email', 'map',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('Nasıl Gidilir', {
            'fields': ('departure_times',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    class Meta:
        model = Contact


@admin.register(LiveSupport)
class LiveSupportAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    class Meta:
        model = LiveSupport

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(RestaurantReservation)
class RestaurantReservationAdmin(admin.ModelAdmin):
    search_fields = ['restaurant', 'name', 'mail', 'phone']
    list_display = ['restaurant', 'name', 'mail', 'phone', 'kvkk1', 'kvkk2', 'created_at']
    list_filter = (
        ('created_at', DateRangeFilter),
        ('restaurant', RelatedFieldListFilter),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(SiteLanguages)
class SiteLanguagesAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'status']
    readonly_fields = ['name', 'code']

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    search_fields = ['email']
    list_display = ['email', 'is_acik_riza_beyani', 'created_at']
    list_filter = (
        ('created_at', DateRangeFilter),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(SubscriberContact)
class SubscriberContactAdmin(admin.ModelAdmin):
    search_fields = ['email', 'name', 'message']
    list_display = ['email', 'name', 'kvkk1', 'kvkk2', 'created_at']
    list_filter = (
        ('created_at', DateRangeFilter),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(SpaReservation)
class SpaReservationAdmin(admin.ModelAdmin):
    search_fields = ['name', 'mail', 'phone']
    list_display = ['name', 'mail', 'phone', 'kvkk1', 'kvkk2', 'created_at']
    list_filter = (
        ('created_at', DateRangeFilter),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CustomTreeItemAdmin(TreeItemAdmin, TranslationAdmin):
    verbose_name = 'MENU'
    verbose_name_plural = 'MENULER'
    # exclude = ('title_en', 'title_tr', 'url_tr', 'url,en')
    """This allows admin contrib to support translations for tree items."""


override_item_admin(CustomTreeItemAdmin)


class GalleryImageAdmin(CompactInline):
    form = ModelWithImageFieldForm
    verbose_name = 'Resim'
    verbose_name_plural = 'Resimler'
    model = GalleryImage
    extra = 0


class GallerySliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    verbose_name = 'Slider'
    verbose_name_plural = 'Slider'
    model = GallerySlider
    extra = 0


class BlogPageSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    verbose_name = 'Slider'
    verbose_name_plural = 'Slider'
    model = BlogPageSlider
    extra = 0


class HomeSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    verbose_name = 'Slider'
    verbose_name_plural = 'Slider'
    model = HomeSlider
    extra = 0


class RoomImageAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = RoomImage
    verbose_name = 'Resim'
    verbose_name_plural = 'Resimler'
    extra = 0


class RoomServiceAdmin(CompactInline):
    model = RoomService
    verbose_name = 'Servis'
    verbose_name_plural = 'Servisler'
    extra = 0
    exclude = ['title', ]


class RoomSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = RoomSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'Slider'
    extra = 0


class DocumentsSliderAdmin(CompactInline):
    model = DocumentsSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'Slider'
    extra = 0


class RestaurantImageAdmin(CompactInline):
    form = ModelWithImageFieldForm2
    model = RestaurantSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'Slider'
    extra = 0


class MenuAdmin(CompactInline):
    model = RestaurantMenu
    verbose_name = 'Menü'
    verbose_name_plural = 'Menüler'
    extra = 0
    fieldsets = (
        ('Genel', {
            'fields': ('status', 'image', 'image_alt_text', 'balance'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'sort_description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'sort_description_en'),
            'classes': ('tab-fs-content',),
        })
    )


class MenuServiceAdmin(CompactInline):
    model = RestaurantService
    verbose_name = 'Servis'
    verbose_name_plural = 'Servisler'
    extra = 0
    exclude = ['title', ]


class MeetingRoomServiceAdmin(CompactInline):
    model = MeetingRoomService
    verbose_name = 'Servis'
    verbose_name_plural = 'Servisler'
    extra = 0
    exclude = ['title', ]


class MeetingRoomSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = MeetingRoomSlider
    verbose_name = 'Slider Resim'
    verbose_name_plural = 'Slider Resimleri'
    extra = 0


class MeetingRoomGalleryAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = MeetingRoomGallery
    verbose_name = 'Resim'
    verbose_name_plural = 'Resimler'
    extra = 0


class SpaFitnessSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = SpaFitnessSlider
    verbose_name = 'Üst Slider'
    verbose_name_plural = 'Üst Slider'
    extra = 0


class SpaFitnessImageAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = SpaFitnessImage
    verbose_name = 'Slider'
    verbose_name_plural = 'Slider'
    extra = 0


class SpaFitnessPropertiesAdmin(CompactInline):
    model = SpaFitnessProperties
    verbose_name = 'Özellik'
    verbose_name_plural = 'Özellikler'
    extra = 0
    exclude = ['title', ]


class SpaFitnessServiceAdmin(CompactInline):
    model = SpaFitnessService
    verbose_name = 'Servis'
    verbose_name_plural = 'Servisler'
    extra = 0
    exclude = ['title', ]


class BlogSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = BlogSlider
    verbose_name = 'Resim'
    verbose_name_plural = 'SLIDER'
    extra = 0


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    inlines = [GalleryImageAdmin, GallerySliderAdmin]

    fieldsets = (
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    class Meta:
        model = Gallery


@admin.register(BlogPage)
class BlogPageAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    inlines = [BlogPageSliderAdmin]

    fieldsets = (
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False



@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [RoomServiceAdmin, RoomImageAdmin, RoomSliderAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'thumbnail_image', 'number_of_people', 'size', 'bed_size'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',)
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'short_description_tr', 'description_tr'),
            'classes': ('tab-fs-content',)
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'short_description_en', 'description_en'),
            'classes': ('tab-fs-content',)
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',)
        }),
    )

    class Meta:
        model = Room


@admin.register(Documents)
class DocumentAdmin(admin.ModelAdmin):
    inlines = [DocumentsSliderAdmin]
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('İçerik (TR)', {
            'fields': ('title_tr', 'description_tr'),
            'classes': ('tab-fs-content',)
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'description_en'),
            'classes': ('tab-fs-content',)
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Blogs)
class BlogAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [BlogSliderAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'image', 'image_alt_text', 'category'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('small_title_tr', 'title_tr', 'short_description_tr', 'description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('small_title_en', 'title_en', 'short_description_en', 'description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    class Meta:
        model = Blogs


@admin.register(Restaurant)
class RestaurantMenuAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [RestaurantImageAdmin, MenuAdmin, MenuServiceAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'base_image', 'logo', 'menu_pdf'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('small_title_tr', 'title_tr', 'short_description_tr', 'description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('small_title_en', 'title_en', 'short_description_en', 'description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    class Meta:
        model = Restaurant


@admin.register(MeetingRoom)
class MeetingRoomAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [MeetingRoomSliderAdmin, MeetingRoomServiceAdmin, MeetingRoomGalleryAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'image', 'image_alt_text', 'plan_pdf'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('small_title_tr', 'title_tr', 'short_description_tr', 'description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('small_title_en', 'title_en', 'short_description_en', 'description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    class Meta:
        model = MeetingRoom


@admin.register(SpaAndFitness)
class SpaFitnessAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [SpaFitnessSliderAdmin, SpaFitnessImageAdmin, SpaFitnessPropertiesAdmin, SpaFitnessServiceAdmin]

    fieldsets = (
        ('İçerik (TR)', {
            'fields': ('title_tr', 'description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Home)
class HomeAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    inlines = [HomeSliderAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('reservation_link',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
        # ('Analytics', {
        #     'fields': ('google_analytics_key', 'yandex_metrics_key',),
        #     'classes': ('tab-fs-tech',),
        # }),
    )

    class Meta:
        model = Home

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Analytics)
class AnalyticsAdmin(admin.ModelAdmin):
    readonly_fields = ['name', ]

    fieldsets = (
        ('Genel', {
            'fields': ('name', 'key', 'link'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
