from django.conf.urls import url
from . import views
from django.urls import path
from django.contrib.sitemaps.views import sitemap

from .sitemaps import StaticViewSitemap, BlogSitemap, RoomSitemap, MeetingRoomSitemap, RestaurantSitemap
from .views import BlogDetail, RoomDetail, RestaurantDetail, MeetingRoomDetail, DocumentDetail

sitemaps = {
    'static': StaticViewSitemap,
    'blog': BlogSitemap,
    'room': RoomSitemap,
    'meeting_room': MeetingRoomSitemap,
    'restaurant': RestaurantSitemap
}


urlpatterns = [
    path('', views.home, name='home'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    path('hakkimizda', views.about, name='about'),
    path('galeri', views.gallery_view, name='galeri'),
    path('blog', views.blog, name='blog'),
    path('iletisim', views.contact, name='contact'),
    path('aboneler', views.subscriber_view, name='subscriber'),
    path('spa', views.spa_fitness_view, name='spa'),
    path('blog/<slug:slug>/', BlogDetail.as_view(), name="blog_detail"),
    path('oda/<slug:slug>/', RoomDetail.as_view(), name="room_detail"),
    path('toplanti/<slug:slug>/', MeetingRoomDetail.as_view(), name="meeting_room_detail"),
    path('restorant/<slug:slug>/', RestaurantDetail.as_view(), name="restaurant_detail"),
    path('belgeler/<slug:slug>/', DocumentDetail.as_view(), name="document_detail"),
    path('clear-cache', views.clear_cache),
]
