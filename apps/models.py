from django.db import models
import os

from django.utils.safestring import mark_safe
from djmoney.models.fields import MoneyField
from meta.models import ModelMeta
from phone_field import PhoneField

from apps.helper.pathcreater import get_file_path, get_upload_path
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

ACTIVE = 1
PASSIVE = 2
PENDING = 2
STATUS_CHOICES = (
    (ACTIVE, 'Active'),
    (PASSIVE, 'Passive'),
)

INSTAGRAM = 1
TWITTER = 2
LINKEDIN = 3
YOUTUBE = 4
FACEBOOK = 5
GMAIL = 6

SOCIAL_MEDIA_TYPE_CHOICES = (
    (INSTAGRAM, 'INSTAGRAM'),
    (TWITTER, 'TWITTER'),
    (LINKEDIN, 'LINKEDIN'),
    (YOUTUBE, 'YOUTUBE'),
    (FACEBOOK, 'FACEBOOK'),
    (GMAIL, 'GMAIL'),
)


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(_('Kayıt Tarihi'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Son Değiştirme Tarihi'), auto_now=True)

    class Meta:
        abstract = True


class MetaModelMixin(models.Model):
    meta_title = models.CharField(_('Meta Title'), max_length=100, default='')
    meta_description = models.TextField(_('Meta Description'), default='')
    other_metas = RichTextUploadingField(_('Diğer Metalar'), default='', null=True, blank=True)

    _metadata = {
        'title': 'title',
        'description': 'description',
        'keywords': 'description',
        'image': 'image',
    }

    def get_meta_title(self):
        if self.meta_title:
            return self.meta_title

    def get_meta_description(self):
        if self.meta_description:
            return self.meta_description

    class Meta:
        abstract = True


class SliderModelMixin(models.Model):
    image = models.FileField(_('Üst Slider Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    def __str__(self):
        return 'Slider' + str(self.pk)

    class Meta:
        abstract = True


class ImageModelMixin(models.Model):
    image = models.FileField(_('Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    class Meta:
        abstract = True


class Home(TimeStampMixin, MetaModelMixin):
    reservation_link = models.URLField(
        _("Rezervasyon Yap Linki"),
        max_length=128,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['-created_at']

        db_table = 'home'
        verbose_name_plural = 'Anasayfa'
        verbose_name = 'Anasayfa'

    def __str__(self):
        return 'Anasayfa'


# Home Slider
class HomeSlider(TimeStampMixin, ImageModelMixin):
    home = models.ForeignKey(Home, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'home_slider'
        verbose_name_plural = 'Anasayfa Slider'
        verbose_name = 'Anasayfa Slider'

    def __str__(self):
        return 'Slider' + str(self.pk)


class About(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    title = models.CharField(_('Başlık'), max_length=60)
    content = RichTextUploadingField(_('İçerik'))
    short_description = RichTextUploadingField(_('Kısa İçerik'), default='')

    class Meta:
        ordering = ['-created_at']

        db_table = 'about'
        verbose_name_plural = 'Hakkımızda'
        verbose_name = 'Hakkımızda'

    def __str__(self):
        return self.title


class AboutSlider(TimeStampMixin, SliderModelMixin):
    about = models.ForeignKey(About, default=None, on_delete=models.CASCADE)


class Room(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=100, blank=True)
    slug = models.SlugField(_('Slug'), max_length=100)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    number_of_people = models.SmallIntegerField(_('Kişi Sayısı'), default=1)
    size = models.SmallIntegerField(_('Boyut (m2)'), default=1)
    bed_size = models.SmallIntegerField(_('Yatak Boyut (m2)'), default=1)
    description = RichTextUploadingField(_('Açıklama'), null=True, blank=True)
    short_description = RichTextUploadingField(_('Kısa Açıklama'), null=True, blank=True)
    thumbnail_image = models.FileField(_('Küçük Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    class Meta:
        ordering = ['-created_at']

        db_table = 'room'
        verbose_name = 'Oda'
        verbose_name_plural = 'Odalar'

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class RoomImage(TimeStampMixin, ImageModelMixin):
    room = models.ForeignKey(Room, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return 'Resim' + str(self.pk)


class RoomSlider(TimeStampMixin, SliderModelMixin):
    room = models.ForeignKey(Room, default=None, on_delete=models.CASCADE)


class RoomService(TimeStampMixin, ImageModelMixin):
    room = models.ForeignKey(Room, default=None, on_delete=models.CASCADE)
    title = models.CharField(_('Başlık'), max_length=60)

    def __str__(self):
        return 'Servis' + str(self.pk)


class Restaurant(TimeStampMixin, MetaModelMixin):
    small_title = models.CharField(_('Ana Sayfa Başlık'), max_length=100, blank=True)
    title = models.CharField(_('Başlık'), max_length=100, blank=True)
    slug = models.SlugField(_('Slug'), max_length=100)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    short_description = RichTextUploadingField(_('Kısa Açıklama'), default='')
    description = RichTextUploadingField(_('Açıklama'), default='')
    logo = models.FileField(_('Logo'), upload_to=get_file_path, null=True, blank=True)
    menu_pdf = models.FileField(_('Menu Pdf'), upload_to=get_upload_path, blank=True)
    base_image = models.FileField(_('Ana Sayfa Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    class Meta:
        ordering = ['-created_at']

        db_table = 'restaurant_menu'
        verbose_name_plural = 'Restoranlar'
        verbose_name = 'Restoran'

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.small_title


class RestaurantMenu(TimeStampMixin, ImageModelMixin):
    restaurant = models.ForeignKey(Restaurant, default=None, on_delete=models.CASCADE)
    title = models.CharField(_('Başlık'), max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    balance = MoneyField(_('Fiyat'), max_digits=14, decimal_places=3, null=True, blank=True)
    sort_description = RichTextUploadingField(_('Kısa Açıklama'))

    def __str__(self):
        return 'Menu' + str(self.pk)


class RestaurantService(TimeStampMixin, ImageModelMixin):
    restaurant = models.ForeignKey(Restaurant, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=60)

    def __str__(self):
        return 'Servis' + str(self.pk)


class RestaurantSlider(TimeStampMixin, SliderModelMixin):
    restaurant = models.ForeignKey(Restaurant, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)


class RestaurantReservation(TimeStampMixin):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name = models.CharField(_('Ad Soyad'), max_length=100, blank=True)
    mail = models.EmailField(_('E-posta'), max_length=100, blank=True)
    phone = PhoneField(_('Telefon Numarası'), blank=True, help_text='Telefon Numarası Giriniz')
    kvkk1 = models.BooleanField(_('Kvkk Metni Seçili Mi ?'))
    kvkk2 = models.BooleanField(_('Açık Rıza Metni Seçili Mi ?'))
    message = models.TextField(editable=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'restaurant_reservation'
        verbose_name_plural = 'Restoran Rezervasyon'
        verbose_name = 'Restoran Rezervasyon'

    def __str__(self):
        return self.name + " (" + str(self.phone) + ")"


class SpaReservation(TimeStampMixin):
    name = models.CharField(_('Ad Soyad'), max_length=100, blank=True)
    mail = models.EmailField(_('E-posta'), max_length=100, blank=True)
    phone = PhoneField(_('Telefon Numarası'), blank=True, help_text='Telefon Numarası Giriniz')
    kvkk1 = models.BooleanField(_('Kvkk Metni Seçili Mi ?'))
    kvkk2 = models.BooleanField(_('Açık Rıza Metni Seçili Mi ?'))
    message = models.TextField(editable=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'spa_reservation'
        verbose_name_plural = 'Spa Rezervasyon'
        verbose_name = 'Spa Rezervasyon'

    def __str__(self):
        return self.name + " (" + str(self.phone) + ")"


class MeetingRoom(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    small_title = models.CharField(_('Menü Başlık'), max_length=100, blank=True)
    title = models.CharField(_('Başlık'), max_length=100, blank=True)
    slug = models.SlugField(_('Slug'), max_length=100)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    plan_pdf = models.FileField(_('Salon Planı'), upload_to=get_upload_path, blank=True)
    description = RichTextUploadingField(_('Açıklama'), default='')
    short_description = RichTextUploadingField(_('Kısa Açıklama'), default='')

    class Meta:
        ordering = ['-created_at']

        db_table = 'meeting'
        verbose_name_plural = 'Toplantı Odaları'
        verbose_name = 'Toplantı Odası'

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.small_title


class MeetingRoomSlider(TimeStampMixin, SliderModelMixin):
    meeting = models.ForeignKey(MeetingRoom, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)


class MeetingRoomService(TimeStampMixin, ImageModelMixin):
    meeting = models.ForeignKey(MeetingRoom, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=60)

    def __str__(self):
        return 'Servis' + str(self.pk)


class MeetingRoomGallery(TimeStampMixin, ImageModelMixin):
    meeting = models.ForeignKey(MeetingRoom, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=60)

    def __str__(self):
        return 'Resim' + str(self.pk)


class SpaAndFitness(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=100)
    description = RichTextUploadingField(_('Açıklama'))
    slug = models.SlugField(_('Slug'), max_length=100)

    class Meta:
        ordering = ['-created_at']

        db_table = 'spa_fitness'
        verbose_name_plural = 'Spa & Fitness'
        verbose_name = 'Spa & Fitness'

    def __str__(self):
        return self.title


class AbstractSpaFitness(TimeStampMixin):
    spa_fitness = models.ForeignKey(SpaAndFitness, default=None, on_delete=models.CASCADE)
    title = models.CharField(_('Başlık'), max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        abstract = True


class SpaFitnessImage(TimeStampMixin, ImageModelMixin):
    spa_fitness = models.ForeignKey(SpaAndFitness, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    def __str__(self):
        return 'Resim' + str(self.pk)


class SpaFitnessSlider(TimeStampMixin, SliderModelMixin):
    spa_fitness = models.ForeignKey(SpaAndFitness, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)


class SpaFitnessProperties(AbstractSpaFitness, ImageModelMixin):

    def __str__(self):
        return 'Özellik' + str(self.pk)


class SpaFitnessService(AbstractSpaFitness, ImageModelMixin):

    def __str__(self):
        return 'Servis' + str(self.pk)


class Blogs(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    small_title = models.CharField(_('Kısa Başlık'), max_length=100)
    title = models.CharField(_('Başlık'), max_length=100)
    slug = models.SlugField(_('Slug'), max_length=100)
    description = RichTextUploadingField(_('Açıklama'))
    short_description = RichTextUploadingField(_('Kısa Açıklama'), default='')
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    category = models.CharField(_('Kategori'), max_length=200, default='')

    class Meta:
        ordering = ['-created_at']

        db_table = 'blogs'
        verbose_name_plural = 'Bloglar'
        verbose_name = 'Blog'

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class BlogSlider(TimeStampMixin, SliderModelMixin):
    blog = models.ForeignKey(Blogs, default=None, on_delete=models.CASCADE)


class Gallery(TimeStampMixin, MetaModelMixin):
    slug = models.SlugField(_('Slug'), max_length=40)

    class Meta:
        ordering = ['-created_at']

        db_table = 'gallery'
        verbose_name_plural = 'Galeri'
        verbose_name = 'Galeri'

    def __str__(self):
        return 'Galeri'


class BlogPage(TimeStampMixin, MetaModelMixin):
    slug = models.SlugField(_('Slug'), max_length=40)

    class Meta:
        ordering = ['-created_at']

        db_table = 'blog_page'
        verbose_name_plural = 'Blog'
        verbose_name = 'Blog'

    def __str__(self):
        return 'Blog'


class BlogPageSlider(TimeStampMixin, SliderModelMixin):
    blog_page = models.ForeignKey(BlogPage, default=None, on_delete=models.CASCADE)


class GallerySlider(TimeStampMixin, SliderModelMixin):
    gallery = models.ForeignKey(Gallery, default=None, on_delete=models.CASCADE)


class GalleryImage(TimeStampMixin, ImageModelMixin):
    gallery = models.ForeignKey(Gallery, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return 'Resim' + str(self.pk)


class Contact(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=100, blank=True)
    description = RichTextUploadingField(_('Açıklama'), null=True, blank=True)
    address = RichTextUploadingField(_('Adress'), null=True, blank=True)
    departure_times = RichTextUploadingField(_('Kalkış Zamanları'))
    country_code = models.CharField(_('Ülke Kodu'), max_length=5, null=True)
    phone = models.CharField(_('Telefon'), max_length=10)
    email = models.EmailField(_('E-posta'), max_length=50)
    map = RichTextUploadingField(_('Harita'))
    city = models.CharField(_('Şehir'), max_length=40)
    check_in = models.CharField(_('Giriş'), max_length=40)
    check_out = models.CharField(_('Çıkış'), max_length=40)

    class Meta:
        ordering = ['-created_at']

        db_table = 'contact'
        verbose_name_plural = 'İletişim'
        verbose_name = 'İletişim'

    def __str__(self):
        return self.title


class ContactSlider(TimeStampMixin, ImageModelMixin):
    contact = models.ForeignKey(Contact, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return 'Slider' + str(self.pk)


class LiveSupport(TimeStampMixin):
    title = models.CharField(_("Canlı Destek Adı"), max_length=60)
    url = models.CharField(_("Canlı Destek Url"), max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'live_support'
        verbose_name_plural = 'Canlı Destek'
        verbose_name = 'Canlı Destek'

    def __str__(self):
        return self.title


class Documents(TimeStampMixin):
    slug = models.SlugField(_('Seo'), unique=True, max_length=100, editable=False)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=100)
    description = RichTextUploadingField(_("Metni İçerik"))

    class Meta:
        ordering = ['-created_at']

        db_table = 'documents'
        verbose_name_plural = 'Belgeler'
        verbose_name = 'Belge'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Documents, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class DocumentsSlider(TimeStampMixin, SliderModelMixin):
    document = models.ForeignKey(Documents, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=60)


class Subscriber(TimeStampMixin):
    email = models.EmailField(_("E-posta"), unique=True)
    is_acik_riza_beyani = models.BooleanField(_("Açık Rıza Metni Seçili Mi ?"), default=False)

    class Meta:
        ordering = ['-created_at']

        db_table = 'subscirber'
        verbose_name_plural = 'Aboneler'
        verbose_name = 'Abone'

    def __str__(self):
        return self.email


class SubscriberContact(TimeStampMixin):
    name = models.CharField(_('Ad Soyad'), max_length=100, blank=True)
    email = models.EmailField(_('E-posta'), max_length=100, blank=True)
    message = models.TextField(_('Mesaj'), editable=True)
    kvkk1 = models.BooleanField(_("KVKK Metni Seçili Mi ?"), default=True)
    kvkk2 = models.BooleanField(_("Açık Rıza Metni Seçili Mi ?"), default=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'subscirber_contact'
        verbose_name_plural = 'İletişim Kayıt'
        verbose_name = 'İletişim Kayıt'

    def __str__(self):
        return self.email


class Social(TimeStampMixin):
    title = models.CharField(_('Başlık'), max_length=100, blank=True)
    icon = models.CharField(_('İcon'), max_length=100, blank=True)
    order_no = models.SmallIntegerField(_('Sıra No'))
    link = models.URLField(
        _("Link"),
        max_length=256,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['id']

        db_table = 'social_media'
        verbose_name_plural = 'Sosyal Medya'

    def __str__(self):
        return self.title


class SiteLanguages(TimeStampMixin):
    name = models.CharField(_('İsim'), max_length=100, blank=True)
    code = models.CharField(_('Kodu'), max_length=100, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['id']

        db_table = 'site_languages'
        verbose_name_plural = 'Site Dilleri'
        verbose_name = 'Site Dili'

    def __str__(self):
        return self.name


class Analytics(TimeStampMixin):
    name = models.CharField(_('İsim'), unique=True, max_length=100, blank=True)
    key = models.CharField(_('Key'), max_length=150, default='')
    link = models.URLField(
        _("Link"),
        max_length=128,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['id']

        db_table = 'analytics'
        verbose_name_plural = 'Analytics'
        verbose_name = 'Analytics'

    def __str__(self):
        return self.name
