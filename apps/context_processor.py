from django.conf import settings
from django.template.loader import render_to_string

from apps.models import Analytics


def analytics(request):
    """
    Returns analytics code.
    """
    return {
        'google_analytics_code': render_to_string("analytics/google_analytics.html", {
            'google_analytics_key': Analytics.objects.get(name='Google').key
        }),
        'yandex_metrics_code': render_to_string("analytics/yandex_metrics.html", {
            'yandex_metrics_key': Analytics.objects.get(name='Yandex').key
        })
    }
