from django import template

from apps.models import Social, Documents, Contact, LiveSupport, ACTIVE, Home, SiteLanguages

register = template.Library()


@register.simple_tag()
def social_media():
    return Social.objects.filter().order_by('order_no')


@register.simple_tag()
def document_kvkk():
    return Documents.objects.get(slug='kvkk')

@register.simple_tag()
def document_gizlilik_politikasi():
    return Documents.objects.get(slug='gizlilik-politikas')


@register.simple_tag()
def document_cerez_politikasi():
    return Documents.objects.get(slug='cerez-politikas')


@register.simple_tag()
def document_sartlar_ve_kosullar():
    return Documents.objects.get(slug='sartlar-ve-kosullar')


@register.simple_tag()
def document_acik_riza_beyani():
    return Documents.objects.get(slug='ack-rza-beyan')

@register.simple_tag()
def contacts():
    return Contact.objects.filter().first()


@register.simple_tag()
def live_supports():
    return LiveSupport.objects.filter(status=ACTIVE).first()


@register.simple_tag()
def get_reservation_link():
    return Home.objects.values('reservation_link').first()['reservation_link']


@register.simple_tag()
def get_site_languages():
    return SiteLanguages.objects.filter(status=ACTIVE)
