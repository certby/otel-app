import datetime

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from .models import *
from django.shortcuts import get_object_or_404, render, redirect
from django.shortcuts import (
    render_to_response
)
from django.template import RequestContext


def home(request):
    homes = Home.objects.get()
    slider = HomeSlider.objects.filter(status=ACTIVE).order_by('id')
    about = About.objects.get()
    layovers = Room.objects.filter(status=ACTIVE).order_by('id')
    eat_and_drinks = Restaurant.objects.filter(status=ACTIVE).order_by('id')
    meeting_rooms = MeetingRoom.objects.filter(status=ACTIVE).order_by('id')
    documents = Documents.objects.filter(status=ACTIVE).order_by('id')
    return render(request, 'home.html', {
        'home': homes,
        'slider': slider,
        'about': about,
        'layovers': layovers,
        'eat_and_drinks': eat_and_drinks,
        'meeting_rooms': meeting_rooms,
        'documents': documents,
    })


def about(request):
    about = About.objects.get()
    about_slider = AboutSlider.objects.filter(about=about)
    return render(request, 'about.html', {
        'about': about,
        'about_slider': about_slider
    })


def gallery_view(request):
    gallery = get_object_or_404(Gallery, slug=request.path.split('/')[2])
    gallery_images = GalleryImage.objects.filter(gallery=gallery)
    gallery_slider = GallerySlider.objects.filter(gallery=gallery)
    return render(request, 'gallery.html', {
        'gallery': gallery,
        'gallery_images': gallery_images,
        'gallery_slider': gallery_slider,
    })


def not_found(request):
    return render(request, 'not_found.html')


@csrf_exempt
def spa_fitness_view(request):
    if request.method == 'POST':
        name = request.POST.get('name', None)
        phone = request.POST.get('phone', None)
        email = request.POST.get('email', None)
        message = request.POST.get('message', None)

        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = SpaReservation.objects.filter(created_at__gte=created_time).count()

        if old_objects < 10:
            res = SpaReservation()
            res.name = name
            res.mail = email
            res.phone = phone
            res.message = message
            res.kvkk1 = True
            res.kvkk2 = True
            res.save()

    spa_fitness = get_object_or_404(SpaAndFitness, slug='spa')
    spa_fitness_slider = SpaFitnessSlider.objects.filter(spa_fitness=spa_fitness, status=ACTIVE)
    spa_fitness_image = SpaFitnessImage.objects.filter(spa_fitness=spa_fitness, status=ACTIVE)
    spa_fitness_properties = SpaFitnessProperties.objects.filter(spa_fitness=spa_fitness, status=ACTIVE)
    spa_fitness_service = SpaFitnessService.objects.filter(spa_fitness=spa_fitness, status=ACTIVE)

    return render(request, 'spa_fitness.html', {
        'spa_fitness': spa_fitness,
        'spa_fitness_slider': spa_fitness_slider,
        'spa_fitness_image': spa_fitness_image,
        'spa_fitness_properties': spa_fitness_properties,
        'spa_fitness_service': spa_fitness_service
    })


@csrf_exempt
def subscriber_view(request):
    if request.method == 'POST':
        email = request.POST.get('mail', '')
        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = Subscriber.objects.filter(created_at__gte=created_time).count()
        if old_objects < 10:
            subs = Subscriber.objects.filter(email=email)
            if not subs:
                subscriber = Subscriber()
                subscriber.email = email
                subscriber.is_acik_riza_beyani = True
                subscriber.save()

        return HttpResponseRedirect(reverse('home'))


def blog(request):
    blog_page = get_object_or_404(BlogPage, slug=request.path.split('/')[2])
    blog_slider = BlogPageSlider.objects.filter(blog_page=blog_page)
    blogs = Blogs.objects.filter(status=ACTIVE).order_by('id')
    return render(request, 'blog.html', {
        'blogs': blogs,
        'blog_page': blog_page,
        'blog_slider': blog_slider,
    })


@csrf_exempt
def contact(request):
    if request.method == 'POST':
        email = request.POST.get('email', '')
        name = request.POST.get('name', '')
        message = request.POST.get('message', '')

        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = SubscriberContact.objects.filter(created_at__gte=created_time).count()

        if old_objects < 10:
            subscriber = SubscriberContact()
            subscriber.name = name
            subscriber.email = email
            subscriber.message = message
            subscriber.kvkk1 = True
            subscriber.kvkk2 = True
            subscriber.save()

        return HttpResponseRedirect(reverse('contact'))

    item = Contact.objects.get()
    contact_slider = ContactSlider.objects.filter(contact=item)
    return render(request, 'contact.html', {
        'item': item,
        'contact_slider': contact_slider

    })


class BlogDetail(DetailView):
    model = Blogs
    template_name = '../templates/blog_detail.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        blog = self.object
        blog_slider = BlogSlider.objects.filter(blog=blog)

        context['blog'] = blog
        context['blog_slider'] = blog_slider

        return context


class RoomDetail(DetailView):
    model = Room
    template_name = '../templates/room.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        room = self.object
        room_images = RoomImage.objects.filter(room=room)
        room_services = RoomService.objects.filter(room=room)
        galleries = Gallery.objects.filter()[:3]
        other_rooms = Room.objects.filter(status=ACTIVE).exclude(id=room.id)[:3]
        room_slider = RoomSlider.objects.filter(room=room)

        context['room'] = room
        context['room_images'] = room_images
        context['room_services'] = room_services
        context['galleries'] = galleries
        context['other_rooms'] = other_rooms
        context['room_slider'] = room_slider

        return context


class MeetingRoomDetail(DetailView):
    model = MeetingRoom
    template_name = '../templates/meeting_room.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        meeting_room = self.object

        meeting_room_service = MeetingRoomService.objects.filter(status=ACTIVE, meeting=meeting_room)
        meeting_room_gallery = MeetingRoomGallery.objects.filter(status=ACTIVE, meeting=meeting_room)
        meeting_room_slider = MeetingRoomSlider.objects.filter(status=ACTIVE, meeting=meeting_room)

        context['meeting_room'] = meeting_room
        context['meeting_room_service'] = meeting_room_service
        context['meeting_room_gallery'] = meeting_room_gallery
        context['meeting_room_slider'] = meeting_room_slider

        return context


class RestaurantDetail(DetailView):
    model = Restaurant
    template_name = '../templates/restaurant.html'
    slug_field = 'slug'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RestaurantDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        restaurant = self.object
        restaurant_menus = RestaurantMenu.objects.filter(status=ACTIVE, restaurant=restaurant)
        restaurant_images = RestaurantSlider.objects.filter(status=ACTIVE, restaurant=restaurant)
        restaurant_services = RestaurantService.objects.filter(status=ACTIVE, restaurant=restaurant)

        context['restaurants'] = restaurant
        context['restaurant_menus'] = restaurant_menus
        context['restaurant_images'] = restaurant_images
        context['restaurant_services'] = restaurant_services

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = super(RestaurantDetail, self).get_context_data(**kwargs)

        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        phone = request.POST.get('phone', None)
        message = request.POST.get('message', None)

        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = RestaurantReservation.objects.filter(created_at__gte=created_time).count()

        if old_objects < 10:
            res = RestaurantReservation()
            res.restaurant = self.object
            res.name = name
            res.mail = email
            res.phone = phone
            res.message = message
            res.kvkk1 = True
            res.kvkk2 = True
            res.save()

        restaurant_menus = RestaurantMenu.objects.filter(status=ACTIVE, restaurant=self.object)
        restaurant_images = RestaurantSlider.objects.filter(status=ACTIVE, restaurant=self.object)
        restaurant_services = RestaurantService.objects.filter(status=ACTIVE, restaurant=self.object)

        context['restaurants'] = self.object
        context['restaurant_menus'] = restaurant_menus
        context['restaurant_images'] = restaurant_images
        context['restaurant_services'] = restaurant_services

        return self.render_to_response(context=context)


class DocumentDetail(DetailView):
    model = Documents
    template_name = '../templates/document.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        document = self.object
        document_slider = DocumentsSlider.objects.filter(document=document)

        context['document'] = document
        context['document_slider'] = document_slider

        return context


@never_cache
def clear_cache(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    cache.clear()
    return HttpResponse('Cache has been cleared')


# HTTP Error 400
def bad_request(request):
    response = render_to_response(
        '400.html',
        context_instance=RequestContext(request)
    )

    response.status_code = 400

    return response

def custom_page_not_found_view(request, exception):
    return render(request, "errors/404.html", {})

def custom_error_view(request, exception=None):
    return render(request, "errors/500.html", {})

def custom_permission_denied_view(request, exception=None):
    return render(request, "errors/403.html", {})

def custom_bad_request_view(request, exception=None):
    return render(request, "errors/400.html", {})
